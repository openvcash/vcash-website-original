    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation </span>
        <span class="icon-bar">
        </span>
        <span class="icon-bar">
        </span>
        <span class="icon-bar">
        </span>
        </button>
        <a class="navbar-brand" href="index.php">Vcash </a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li>
          <a href="getting_started.php">Getting Started </a>
          </li>
          <li>
          <a href="wallets.php">Wallets </a>
          </li>
          <li>
          <a href="news.php">News </a>
          </li>
          <li>
          <a href="faq.php">FAQ </a>
          </li>
          <li>
          <a href="network.php">Network </a>
          </li>
          <li class="dropdown">
          <a href="papers/vanillacoin.pdf" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Resources <span class="caret">
          </span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li>
            <a href="https://docs.vcash.info">Docs </a>
            </li>
            <li>
            <a href="papers/vanillacoin.pdf">White Paper </a>
            </li>
            <li>
            <a href="https://explorer.vchain.info">xCore's Explorer </a>
            </li>
            <li>
            <a href="http://explorer.vcashproject.org">Wuher's Explorer </a>
            </li>
            <li>
            <a href="https://www.blockexperts.com/xvc">Blockexperts's Explorer </a>
            </li>
          </ul>
          </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Community <span class="caret">
          </span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li>
            <a href="https://forum.vcash.info">Forum </a>
            </li>
            <li>
            <a href="https://blog.vcash.info" target="_blank">Blog </a>
            </li>
            <li>
            <a href="https://twitter.com/Vcashinfo" target="_blank">Twitter </a>
            </li>
            <li>
            <a href="https://www.reddit.com/r/Vcash/">Reddit </a>
            </li>
            <li>
            <a href="https://vcash.slack.com">Slack </a>
            </li>
            <li>
            <a href="https://slack.vcash.info">Slack Invites </a>
            </li>
          </ul>
          </li>
        </ul>
      </div>
    </div>
    </nav>