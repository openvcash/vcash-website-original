  <div class="alert alert-dismissable alert-warning">
    <span class="glyphicon glyphicon-warning-sign">
    </span>
    <button type="button" class="close" data-dismiss="alert">× </button> Version 0.6.0.2 out, please upgrade your nodes. Current 0.5.1 users will have to resynch the chain.
  </div>