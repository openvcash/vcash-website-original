<?php

	$platform = $_GET["platform"];

	$msg = "Please wait...";

	$url = "";

	if ($platform == "osx")
	{
		$url = "/downloads/Vcash.dmg";
	}
	else if ($platform == "win64")
	{
		$url = "/downloads/Vcash_x64.zip";
	}
	else if ($platform == "win32d")
	{
		$url = "/downloads/vcashd_x86.zip";
	}
	else if ($platform == "win64d")
	{
		$url = "/downloads/vcashd_x64.zip";
	}
	else if ($platform == "linux")
	{
		$url = "https://github.com/openvcash/vcash";
		$msg = "Redirecting to github.com/openvcash/vcash.";
	}
	else if ($platform == "github")
	{
		$url = "https://github.com/openvcash/vcash";
		$msg = "Redirecting to github.com/openvcash/vcash.";
	}

	echo "<meta http-equiv=\"refresh\" content=\"1;URL='" . $url . "'\">";

	echo $msg;
?>